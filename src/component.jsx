import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

class Component extends React.PureComponent {
  render() {
    return null;
  }
}

const mapStateToProps = () => ({});

export default withRouter(
  connect(
    mapStateToProps,
    // Import suggestions work for workingFunction, but not for functionToTest.
    // If you comment out functionToTest in another file, the import suggestions will start to work again.
    { functionToTest, workingFunction }
  )(Component)
);
