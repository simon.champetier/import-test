import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

class Component extends React.PureComponent {
  render() {
    return null;
  }
}

const mapStateToProps = () => ({});

const Component7 = compose(
  connect(
    mapStateToProps,
    {
      functionToTest,
    }
  ),
  Component
);

export default Component7;
