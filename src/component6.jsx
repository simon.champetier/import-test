import React from 'react';
import { connect } from 'react-redux';

class Component extends React.PureComponent {
  render() {
    return null;
  }
}

const mapStateToProps = () => ({});

const Component6 = connect(
  mapStateToProps,
  {
    functionToTest,
  }
)(Component);

export default Component6;
