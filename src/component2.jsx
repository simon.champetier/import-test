import React from 'react';
import { connect } from 'react-redux';

class Component extends React.PureComponent {
  render() {
    return null;
  }
}

const mapStateToProps = () => ({});

export default connect(
  mapStateToProps,
  {
    functionToTest,
  }
)(Component);
