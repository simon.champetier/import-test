import React from 'react';
import { connect } from 'react-redux';

export class Component extends React.PureComponent {
  render() {
    return null;
  }
}

const mapStateToProps = () => ({});

const connectedInventoryItemGeneralInformation = connect(
  mapStateToProps,
  {
    functionToTest,
  }
)(Component);

export default connectedInventoryItemGeneralInformation;
