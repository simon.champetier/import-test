import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

class Component extends React.PureComponent {
  render() {
    return null;
  }
}

const mapStateToProps = () => ({});

export default withRouter(
  connect(
    mapStateToProps,
    {
      functionToTest,
    }
  )(Component)
);
